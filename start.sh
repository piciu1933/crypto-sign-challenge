#/bin/sh

# install python
python3 --version
if [ $? -ne 0 ]; then
    apk add python3=3.8.10-r0
    echo "[INSTALLED] `python3 --version`"
fi

# install openssl
openssl version
if [ $? -ne 0 ]; then
    apk add openssl=1.1.1l-r0
    echo "[INSTALLED] `openssl version`"
fi

if [ ! -f "$HOME/.local/share/signer/private_key.pem" ]; then
    echo "$HOME/.local/share/signer/private_key.pem not found"

    # make $HOME/.local/share/signer directory
    cd $HOME
    mkdir -p .local/share/signer
    
    # generate rsa private key in $HOME/.local/share/signer
    openssl genrsa -out $HOME/.local/share/signer/private_key.pem
fi
