from os import popen, system, path
from base64 import b64encode
from json import dumps
from sys import argv


def generate_public_key(private_key_path:str) -> str:
    return popen(f'openssl rsa -in {private_key_path} -pubout -outform PEM 2>/dev/null').read()


def save_message_to_file(message:str, temp_msg_file_name:str="tmp_msg.txt") -> str:
    current_pwd = popen('pwd').read()
    current_path = current_pwd.rstrip()
    temp_msg_file_path = path.join(current_path, temp_msg_file_name)

    with open(temp_msg_file_path, "w") as msg_file:
        msg_file.write(message)

    return temp_msg_file_path


def sign_message_file(temp_msg_file_path:str, private_key_path:str, temp_signature_file_path:str='./signature') -> str:
    system(f'openssl dgst -sha256 -sign {private_key_path} -out {temp_signature_file_path} {temp_msg_file_path}')

    with open(temp_signature_file_path, 'rb') as sign_file:
        signature = sign_file.read()
    
    system(f'rm {temp_msg_file_path}')
    system(f'rm {temp_signature_file_path}')

    return b64encode(signature).decode()


def sign_message(message:str, private_key_path:str='$HOME/.local/share/signer/private_key.pem') -> str:
    public_key = generate_public_key(private_key_path)
    temp_msg_file_path = save_message_to_file(message)
    signature = sign_message_file(temp_msg_file_path, private_key_path)

    response = {
        "message": message,
        "signature": signature,
        "pubkey": public_key
    }

    return dumps(response)


if __name__ == "__main__":
    try:
        message = argv[1]
    except IndexError:
        message = ""

    if len(message) > 250:
        exit('The message is too long. Max. 250 characters are allowed!')
    print(sign_message(message))
