import unittest
import json
from os import popen

import execute

class DefaultTestCase(unittest.TestCase):
    def test_with_dynamic_private_key(self):
        tested_result_beginning = '{"message": "test_with_dynamic_private_key", "signature": '
        message = "test_with_dynamic_private_key"

        try:
            cmd_result = popen(f"crypto-sign-challenge {message}").read()
        except ResourceWarning:
            cmd_result = execute.sign_message(message)

        self.assertEqual(cmd_result[:len(tested_result_beginning)], tested_result_beginning)


    def test_with_dynamic_private_key_when_msg_empty(self):
        tested_result_beginning = '{"message": "", "signature": '

        try:
            cmd_result = popen("crypto-sign-challenge").read()
        except ResourceWarning:
            cmd_result = execute.sign_message("")

        self.assertEqual(cmd_result[:len(tested_result_beginning)], tested_result_beginning)


    def test_with_static_private_key_when_msg_empty(self):
        tested_result = '{"message": "", "signature" : "On5MqDlK6cOVsfUZbm6BQL7eVF5qQmAZ/7R4D8dZW0OaTelwdJ68dpmkti3TkS3G+j5BC+RFcfDozom4e3LdaHcK6uXmAg4gMQLHDmYxyqwniqLKqjcL7Bn11BwVKQD/3YsEZdxvYr3iDWUMe5QJKQjUWgrsF7epCpMqTgqIrsi7V39l6BZxXrl0wEWK+CgTL8HCudkG0r13JmCWnsqJG9Gc01nw6QbjII3yCjzazh/bnMx9cTyqbd2Y19EDGHWHB8mxfDTH8h9eLNdlTn74reCmwh6q8v+tes557Zyc0DDMBV4eJRO+pojl8fD7SZS0brOz9kjvP52GJtSKWzo88Q==", "pubkey": "-----BEGIN PUBLIC KEY-----\\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAykPoCUgcaIsTC1DCF6ul\\nyXwj+v532iU3sMkPFdlLbbNSSxleiU9LUtW3ZbWlYtfL7414W+IHpUU7wCKdtyVS\\nF9oANH1FEJH4LTAzLpli+7mFWqKaENkACD9tE+2Reja9O4CAtVRpX2jv+kkW0Q3s\\nYZCA65KAum7mTrdh8S1/inETzkSugqhta7ZdeRcziyvHBuC8+oiIrzuk86q5h4qp\\nOOmYYf/ESrYcdv0nvbOqcKI48UjfkJSRSst68U12VhP9yu/DLwIj2LlzAw9XqNQD\\nvU1K/qlrnTERyabw5QWO0iD6yz+0Tliq46zQSbvp3r6mqTQNatQ9EyAU5XeIeC5Z\\nFQIDAQAB\\n-----END PUBLIC KEY-----\\n"}'
        priv_key_path = "priv-test-key.pem"

        cmd_result = execute.sign_message("", private_key_path=priv_key_path)

        self.assertEqual(json.loads(cmd_result), json.loads(tested_result))


    def test_answers_schema(self):
        message = "test_answers_schema"
        json_schema_path = "../response_schema.json"

        try:
            from jsonschema import validate
        
            try:
                cmd_result = popen(f"crypto-sign-challenge {message}").read()
            except ResourceWarning:
                cmd_result = execute.sign_message(message)

            try:
                cmd_result_json = json.loads(cmd_result)
            except json.decoder.JSONDecodeError:
                self.fail("Output is not a proper JSON file")
            
            with open(json_schema_path) as json_schema_file:
                try:
                    json_schema_text = json_schema_file.read()

                    try:
                        json_schema_json = json.loads(json_schema_text)
                    except json.decoder.JSONDecodeError:
                        self.fail("Schema file cannot be properly parsed")

                    validate(instance=cmd_result_json, schema=json_schema_json)
                except json.exceptions.ValidationError:
                    self.fail("Response's JSON is not valid according to provided schema")
                except FileNotFoundError:
                    self.skipTest("Schema file cannot be found")
                else:
                    self.assertTrue(True)
        except ModuleNotFoundError:
            self.skipTest("jsonschema is not installed, to install run 'python3 -m pip install -r tests_requirements.txt'")
