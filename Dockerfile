FROM alpine:3.12
ADD ./.ashrc /root/
ADD ./start.sh /home/
ADD ./crypto-sign-challenge /home/crypto-sign-challenge
ENV ENV="/root/.ashrc"
RUN "/home/start.sh"